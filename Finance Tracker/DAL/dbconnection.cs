﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace DAL
{
    public class dbconnection
    {
        //this is used to connect the application to oracle database vai connection string 
        public OracleConnection con = new OracleConnection("Data Source=" + "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = DESKTOP-195NLJ5)(PORT = 1521))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = XE) ) ); User Id  =saurav; password = hello");

        public OracleConnection getCon()
        {
            //check whether the connection is opend or closed
            if (con.State == System.Data.ConnectionState.Closed)
            {
                con.Open();
            }
            return con;
        }

       //creating method to perform select query in the database
        public DataTable ExeReader(OracleCommand cmd)
        {
            cmd.Connection = getCon();
            OracleDataReader dr;
            DataTable dt = new DataTable();
            dr = cmd.ExecuteReader();
            dt.Load(dr);
            con.Close();
            return dt;
        }
        //creating method to peerform add, update and delete in the database
        public int ExNonQuery(OracleCommand cmd)
        {
            cmd.Connection = getCon();
            int row = cmd.ExecuteNonQuery();
            return row;
            
        }
            
       
    }
}
