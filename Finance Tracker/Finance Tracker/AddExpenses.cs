﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
namespace Finance_Tracker
{
    public partial class AddExpenses : UserControl
    {
        public AddExpenses()
        {
            InitializeComponent();
            DisplayTodaysExpense();
        }
        Operations op = new Operations();
        string totalamount, itemname;
        string TransactionID;
       
        private void AddExpenses_Load(object sender, EventArgs e)
        {
           DataTable dt = op.getCategories();
            //population the dropdown box with different types of categories
           for (int i = 0; i < dt.Rows.Count; i++)
           {
              
               showCategories.AddItem(Convert.ToString(dt.Rows[i]["categoryname"]));

           }

           totalExpense();


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void showCategories_onItemSelected(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            double num;

            //to find out whether the entered phone number has a numeric or string value
            bool isPrice = double.TryParse(txtprice.Text.Trim(), out num);
            bool isQuantity = double.TryParse(txtQuantity.Text, out num);

            if (txtexpensename.Text.Equals("") || txtprice.Text.Equals("") || txtprice.Text.Equals("") || txtQuantity.Text.Equals("") || showCategories.selectedIndex<0)
            {
                MessageBox.Show("Please enter missing fields");
            }
            else if (!isPrice || !isQuantity)
            {
                MessageBox.Show("Enter numeric value");
                txtprice.BorderColorFocused = Color.Red;
                txtQuantity.BorderColorFocused = Color.Red;
            }
      
            else
            {
                int a = op.addTransaction(txtexpensename.Text, double.Parse(txtprice.Text) * double.Parse(txtQuantity.Text), txtdate.Value.Year.ToString() + "/" + txtdate.Value.Month.ToString() + "/" + txtdate.Value.Day.ToString() + " 00:00:00 ", showCategories.selectedValue);
                if (a > 0)
                {
                    MessageBox.Show("DATA INSERTED SUCCESFULLY");
                    DisplayTodaysExpense();
                    totalExpense();

                }
            }
        }

        private void btnview_Click(object sender, EventArgs e)
        {
            DisplayTodaysExpense();
        }
        //to display the total expenses of the current day
        private void totalExpense()
        {
            DataTable dt = op.getTotalAmount(DateTime.Now.ToString("yyyy-MM-dd"));
            //displaying total amount in the lable
            lblTotalAmount.Text = dt.Rows[0]["sum"].ToString();
        }
        private void DisplayTodaysExpense()
        {

            DataTable dt = op.showTransaction(DateTime.Now.ToString("yyyy-MM-dd"));//passing current date to method
            dataTodaysExpense.DataSource = dt;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {


            try
            {

                DataTable dt = op.showTransaction(DateTime.Now.ToString("yyyy-MM-dd"));
                if (dt.Rows.Count < 1)
                {
                    MessageBox.Show("No data in the table");
                }
                else
                {
                    TransactionID = dataTodaysExpense.SelectedCells[0].Value.ToString();//get the transaction id from the datagrid view
                    int deleteData = op.deleteTransaction(TransactionID);//passing the id to the bll layer
                    if (deleteData > 0)
                    {
                        DisplayTodaysExpense();
                        totalExpense();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = op.showTransaction(DateTime.Now.ToString("yyyy-MM-dd"));
                if (dt.Rows.Count < 1)
                {
                    MessageBox.Show("No data in the table");
                }
                else
                {
                    itemname = dataTodaysExpense.SelectedCells[1].Value.ToString();
                    totalamount = dataTodaysExpense.SelectedCells[2].Value.ToString();
                    TransactionID = dataTodaysExpense.SelectedCells[0].Value.ToString();
                    int isUpdated = op.updateTransaction(TransactionID, itemname, double.Parse(totalamount));
                    if (isUpdated > 0)
                    {
                        MessageBox.Show("DATA UPDATED SUCCESFULLY");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        


        }
       
        private void dataTodaysExpense_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
           
            
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtdate_onValueChanged(object sender, EventArgs e)
        {

        }

       
    }
}
