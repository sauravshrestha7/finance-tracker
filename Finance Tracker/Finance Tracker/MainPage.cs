﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Finance_Tracker
{
    public partial class MainPage : Form
    {
        private Point mousePoint;
        public MainPage()
        {
            InitializeComponent();
            panelHighlight.Top = btnExpenses.Top;
            addExpenses1.BringToFront();
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {

        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {

        }

        private void addExpenses1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void MainPage_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panelHighlight.Height = btnExpenses.Height;
            panelHighlight.Top = btnExpenses.Top;
            addExpenses1.BringToFront();
        }


        private void btncategory_Click(object sender, EventArgs e)
        {
           
        }

        private void expenseCategory2_Load(object sender, EventArgs e)
        {

        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Close();
            Login l = new Login();
            l.Show();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            panelHighlight.Height = btnReport.Height;
            panelHighlight.Top = btnReport.Top;
            report1.BringToFront();
        }

        private void MainPage_MouseDown(object sender, MouseEventArgs e)
        {
            mousePoint = new Point(-e.X, -e.Y);
        }

        private void MainPage_MouseMove(object sender, MouseEventArgs e)
        {
            //move the windows without border 
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point currentPos = Location;
                currentPos.Offset(e.X + mousePoint.X, e.Y + mousePoint.Y);
                this.Location = currentPos;
            }
        }

        private void MainPage_MouseEnter(object sender, EventArgs e)
        {
          
        }

        private void btncategory_Click_1(object sender, EventArgs e)
        {
            panelHighlight.Height = btncategory.Height;
            panelHighlight.Top = btncategory.Top;
            expenseCategory1.BringToFront();
        }
    }
}
