﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace Finance_Tracker
{
    public partial class Login : Form
    {
        Operations op = new Operations();
        DataTable dt = new DataTable();
        private Point mousePoint;
        public Login()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
            
        }

        
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Login_MouseMove(object sender, MouseEventArgs e)
        {
            //move the windows without border 
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point currentPos = Location;
                currentPos.Offset(e.X + mousePoint.X, e.Y + mousePoint.Y);
                this.Location = currentPos;
            }
        }

        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            mousePoint = new Point(-e.X, -e.Y);
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            Operations op = new Operations();
            dt = op.login(txtuserid.Text, txtpassword.Text);

            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("Login success");
                MainPage pg = new MainPage();
                pg.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Username or password wrong");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
