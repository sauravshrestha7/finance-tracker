﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace Finance_Tracker
{
    public partial class ExpenseCategory : UserControl
    {
        public ExpenseCategory()
        {
            InitializeComponent();
        }
        Operations op = new Operations();

        private void ExpenseCategory_Load(object sender, EventArgs e)
        {
          
        }

        private void btnAddcategory_Click(object sender, EventArgs e)
        {

            if (txtcategory.Text.Equals(""))
            {
                MessageBox.Show("Enter Category Name");
            }
            else
            {
                int row = op.addCategory(txtcategory.Text);
                if (row > 0)
                {
                    MessageBox.Show("Data inserted succesfully");
                    txtcategory.Text = "";
                    LoadTable();
                  
                }
            }
        }

      
        public void LoadTable()
        {
            DataTable dt = op.showCategories();
            bunifuCustomDataGrid1.DataSource = dt;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            DataTable dt = op.showCategories();
            if (dt.Rows.Count < 1)
            {
                MessageBox.Show("No data in the table");
            }
            else
            {
                string item;
                item = bunifuCustomDataGrid1.SelectedCells[0].Value.ToString();
                int deleteData = op.deleteCategory(item);
                if (deleteData > 0)
                {
                    LoadTable();
                }
            }


        }

        private void btnView_Click(object sender, EventArgs e)
        {

            DataTable dt = op.showCategories();
            bunifuCustomDataGrid1.DataSource = dt;
        }
    }
}
