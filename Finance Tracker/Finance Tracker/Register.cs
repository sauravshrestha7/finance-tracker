﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using BLL;

namespace Finance_Tracker
{
    public partial class Register : Form
    {
        private Point mousePoint;
        public Operations op = new Operations();
        double income = 0;
        public Register()
        {
            InitializeComponent();
            
        }

        

        private void bunifuMaterialTextbox4_OnValueChanged(object sender, EventArgs e)
        {
            try
            {

                income = double.Parse(txtincome.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                txtincome.Text = "";
            }
        }
        
        private void picClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void picMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            int phonenumber = txtphonenumebr.Text.Length;
            int num;

            //to find out whether the entered phone number has a numeric or string value
            bool isNum = int.TryParse(txtphonenumebr.Text.Trim(), out num);

          //validation for all the text fields and types of data entered by the user
            if (txtfname.Text.Equals("") || txtlastname.Text.Equals(""))
            {
                MessageBox.Show("Please Firstname or lastname");
            }
            else if (txtpassword.Text.Equals("") || txtrepassword.Text.Equals("") || txtrepassword.Text != txtpassword.Text)
            {
                MessageBox.Show("The passwords did not match");
                txtrepassword.LineIdleColor = Color.Red;
                txtpassword.LineIdleColor = Color.Red;
            }
            else if (txtincome.Text.Equals(""))
            {
                MessageBox.Show("Enter income");
             
                txtincome.LineIdleColor = Color.Red;
            }
            else if (txtphonenumebr.Text.Equals("") || !isNum )
            {

                MessageBox.Show("The phone number is not a valid phone number");
                txtphonenumebr.Text.Equals("");
       
                txtphonenumebr.LineIdleColor = Color.Red;
            }
            else if (phonenumber != 10)
            {
                MessageBox.Show("Phone number entered is not a 10 digit number");
            }
            else if (txtaddress.Text.Equals(""))
            {
                MessageBox.Show("Enter address");

                txtaddress.LineIdleColor = Color.Red;
            }



            else
            {
                //calling the method in BLL layer for registering the user
                int a = op.RegisterUser(txtfname.Text + " " + txtlastname.Text, txtpassword.Text, income, txtaddress.Text, txtphonenumebr.Text);
                if (a > 0)
                {
                    MessageBox.Show("Record Added succefully");
                    Login OpenLogin = new Login();
                    OpenLogin.Show();
                }

            }
           
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mousePoint = new Point(-e.X, -e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            //move the windows without border 
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point currentPos = Location;
                currentPos.Offset(e.X + mousePoint.X, e.Y + mousePoint.Y);
                this.Location = currentPos;
            }
        }
    }
}
