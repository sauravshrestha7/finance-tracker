﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace Finance_Tracker
{
    public partial class Report : UserControl
    {
        public Report()
        {
            InitializeComponent();
        }
        Operations op = new Operations();
        private void Report_Load(object sender, EventArgs e)
        {
            displayTransaction();
            totalExpense();
            
           DataTable dt = op.getCategories();
            //population the dropdown box with different types of categories
           for (int i = 0; i < dt.Rows.Count; i++)
           {

               cmbCategory.AddItem(Convert.ToString(dt.Rows[i]["categoryname"]));

           }
        }
        private void totalExpense()
        {
            //adding all the values of totalprice column to get total expenditure of the values in the datagridview

            decimal Total = 0;

            for (int i = 0; i < showTransaction.Rows.Count; i++)
            {
                Total += Convert.ToDecimal(showTransaction.Rows[i].Cells["TOTALPRICE"].Value);
            }

            lbltotal.Text = Total.ToString();
        }
        private void displayTransaction()
        {

            //displaying all transaction in gridview
            DataTable dt = op.displayAllTransaction();
            showTransaction.DataSource = dt;
        }


        private void datepicker_onValueChanged(object sender, EventArgs e)
        {
            string date = datepicker.Value.Year.ToString() + "/" + datepicker.Value.Month.ToString();
            DataTable dt = op.sortDate(date,"");
            showTransaction.DataSource = dt;
            totalExpense();
        }

        private void cmbCategory_onItemSelected(object sender, EventArgs e)
        {
            string date = datepicker.Value.Year.ToString() + "/" + datepicker.Value.Month.ToString();
            DataTable dt = op.sortDate(date,cmbCategory.selectedValue);
            showTransaction.DataSource = dt;
            totalExpense();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            displayTransaction();
            totalExpense();
            cmbCategory.Invalidate();
        }
    }
}
