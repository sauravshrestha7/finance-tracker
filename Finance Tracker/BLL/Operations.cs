﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using DAL;
namespace BLL
{
    public class Operations
    {
        public dbconnection db = new dbconnection();
        public DataTable login(string username, string password)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from tbl_user where username='" + username + "' and password='" + password + "'";
            return db.ExeReader(cmd);
        }

        public int RegisterUser(string username, string password, double income, string address, string phonenumber)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into tbl_user (userid,username,password,income,address,phonenumber) values (seq_userid.nextval,'" + username + "','" + password + "'," + income + ",'" + address + "','" + phonenumber + "')";
            return db.ExNonQuery(cmd);
        }

        public int addCategory(string CategoryName)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into tbl_expensecategory (category_id,CATEGORYNAME) values (seq_userid.nextval,'" + CategoryName + "')";
            return db.ExNonQuery(cmd);
        }
        public DataTable showCategories()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from tbl_expensecategory";
            return db.ExeReader(cmd);
        }
        public int deleteCategory( string categoryID)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "delete from tbl_expensecategory where category_id="+categoryID+"";
            return db.ExNonQuery(cmd);
       
        }
        public DataTable getCategories()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select categoryname from tbl_expensecategory";
            return db.ExeReader(cmd);
        }
        //creating function to add the transaction of each expense
        public int addTransaction(string itemname, double totalprice, string transaction_date, string categoryname)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into expense_transaction (transaction_id,itemname,totalprice,transaction_date,categoryname) values (seq_userid.nextval,'" + itemname + "'," + totalprice + ",TO_DATE('"+transaction_date+"', 'yyyy/mm/dd hh24:mi:ss'),'" + categoryname +"')";
            return db.ExNonQuery(cmd); 
        }

        public DataTable showTransaction(string date)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from expense_transaction where TO_CHAR(transaction_date, 'YYYY-MM-DD') = '"+date+"' ";
            return db.ExeReader(cmd);

        }
        public DataTable getTotalAmount(string date)
        {

            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select SUM(totalprice) as sum from expense_transaction where TO_CHAR(transaction_date, 'YYYY-MM-DD') = '" + date + "'";
            return db.ExeReader(cmd);
        }
        public int deleteTransaction(string transactionId)
        {
              OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "delete from expense_transaction where transaction_id="+transactionId+"";
            return db.ExNonQuery(cmd);
        }
        public int updateTransaction(string transactionID,string ItemName, double totalamount)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "update expense_transaction set itemname='" + ItemName + "',totalprice='" + totalamount + "' where transaction_id=" + transactionID + "";
            return db.ExNonQuery(cmd);
        }
        public DataTable displayAllTransaction()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from expense_transaction";
            return db.ExeReader(cmd);

        }
        public DataTable sortDate(string date,string category)
        {
            OracleCommand cmd = new OracleCommand();
            if (category == "")
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from expense_transaction where TO_CHAR(transaction_date, 'YYYY/MM') = '" + date + "'";
            }
            else
            {

                cmd.CommandText = "select * from expense_transaction where TO_CHAR(transaction_date, 'YYYY/MM') = '" + date + "' and categoryname = '"+category+"'";
            }
            return db.ExeReader(cmd);
        }
        
    }
}
